/*******************************************************************************
 * Copyright (c) 2012 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package org.eclipse.xtend.util.stdlib.tests;

import junit.framework.TestCase;

import org.eclipse.xtend.expression.ExecutionContextImpl;
import org.eclipse.xtend.util.stdlib.GlobalVarExtensions;

/**
 * @author thoms - Initial contribution and API
 */
public class GlobalVarExtensionsTest extends TestCase {
	private ExecutionContextImpl execCtx;
	private GlobalVarExtensions ext;


	@Override
	public void setUp () {
		execCtx = new ExecutionContextImpl();
		ext = new GlobalVarExtensions();
		ext.setExecutionContext(execCtx);
	}

	public void test_GlobalVarAccess () {
		// element not accessed yet, thus global var is null
		assertNull(ext.getGlobalVar("foo"));
		// set a global var value
		ext.storeGlobalVar("foo", "bar");
		// now the global var must be set
		assertEquals("bar", ext.getGlobalVar("foo"));
		// clear the global var
		ext.storeGlobalVar("foo", null);
		assertNull(ext.getGlobalVar("foo"));
	}

}

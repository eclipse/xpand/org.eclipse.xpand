/*******************************************************************************
 * Copyright (c) 2012 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package org.eclipse.xtend.util.stdlib.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

/**
 * @author thoms - Initial contribution and API
 */
public class AllTests {
	public static Test suite() {
		TestSuite suite = new TestSuite("Tests for org.eclipse.xtend.util.stdlib");
		suite.addTestSuite(org.eclipse.xtend.util.stdlib.tests.StatefulExtensionsTest.class);
		suite.addTestSuite(org.eclipse.xtend.util.stdlib.tests.CounterExtensionsTest.class);
		suite.addTestSuite(org.eclipse.xtend.util.stdlib.tests.ElementPropertiesExtensionsTest.class);
		suite.addTestSuite(org.eclipse.xtend.util.stdlib.tests.GlobalVarExtensionsTest.class);
		suite.addTestSuite(org.eclipse.xtend.util.stdlib.tests.UIDHelperTest.class);
		return suite;
	}
}

package org.eclipse.xtend.typesystem.uml2.tests;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.eclipse.core.runtime.Platform;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.URIConverter;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.xtend.typesystem.uml2.Setup;
import org.junit.Test;

public class SetupTest {
	@Test
	public void testURIMapping () {
		new Setup();

		ResourceSet rs = new ResourceSetImpl();
		final URIConverter uriConverter = rs.getURIConverter();
		URI normalized;
		normalized = uriConverter.normalize(URI.createURI("pathmap://UML_PROFILES/UML2.profile.uml"));
		if (Platform.isRunning()) {
			assertTrue(normalized.toString().matches("platform:/plugin/org.eclipse.uml2.uml.resources/profiles/UML2.profile.uml"));
		} else {
			assertTrue(normalized.toString().matches("jar:file:.*/org.eclipse.uml2.uml.resources_.*.jar!/profiles/UML2.profile.uml"));
		}

		normalized = uriConverter.normalize(URI.createURI("pathmap://UML_METAMODELS/UML.metamodel.uml"));
		if (Platform.isRunning()) {
			assertTrue(normalized.toString().matches("platform:/plugin/org.eclipse.uml2.uml.resources/metamodels/UML.metamodel.uml"));
		} else {
			assertTrue(normalized.toString().matches("jar:file:.*/org.eclipse.uml2.uml.resources_.*.jar!/metamodels/UML.metamodel.uml"));
		}

		normalized = uriConverter.normalize(URI.createURI("pathmap://UML_LIBRARIES/UMLPrimitiveTypes.library.uml"));
		if (Platform.isRunning()) {
			assertTrue(normalized.toString().matches("platform:/plugin/org.eclipse.uml2.uml.resources/libraries/UMLPrimitiveTypes.library.uml"));
		} else {
			assertTrue(normalized.toString().matches("jar:file:.*/org.eclipse.uml2.uml.resources_.*.jar!/libraries/UMLPrimitiveTypes.library.uml"));
		}

		normalized = uriConverter.normalize(URI.createURI("http://www.eclipse.org/uml2/4.0.0/UML/Profile/L2"));
		if (Platform.isRunning()) {
			assertTrue(normalized.toString().matches("platform:/plugin/org.eclipse.uml2.uml.resources/profiles/StandardL2.profile.uml")); // see Bug#419090
		} else {
			assertTrue(normalized.toString().matches("jar:file:.*/org.eclipse.uml2.uml.resources_.*.jar!/profiles/StandardL2.profile.uml")); // see Bug#419090
		}
	}

	@Test
	public void testEPackageRegistry () {
		new Setup();

		assertNotNull(EPackage.Registry.INSTANCE.getEPackage("http://www.eclipse.org/uml2/2.0.0/UML"));
		assertNotNull(EPackage.Registry.INSTANCE.getEPackage("http://www.eclipse.org/uml2/2.1.0/UML"));
		assertNotNull(EPackage.Registry.INSTANCE.getEPackage("http://www.eclipse.org/uml2/3.0.0/UML"));
		assertNotNull(EPackage.Registry.INSTANCE.getEPackage("http://www.eclipse.org/uml2/4.0.0/UML"));
	}
}

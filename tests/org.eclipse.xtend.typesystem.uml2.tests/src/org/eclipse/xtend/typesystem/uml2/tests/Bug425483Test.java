/*
 * Copyright (c) 2014 Yatta Solutions GmbH and others.
 * All rights reserved.   This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *   Manuel Bork <bork@yatta.de> (Yatta Solutions GmbH) - initial API and implementation
 */
package org.eclipse.xtend.typesystem.uml2.tests;

import java.net.URL;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.uml2.common.util.UML2Util;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.xtend.expression.TypeSystemImpl;
import org.eclipse.xtend.typesystem.Type;
import org.eclipse.xtend.typesystem.emf.EClassType;
import org.eclipse.xtend.typesystem.uml2.profile.ProfileMetaModel;
import org.eclipse.xtend.typesystem.uml2.profile.StereotypeType;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Test suite for the changes introduced for Bug 425483.
 *
 * @see https://bugs.eclipse.org/bugs/show_bug.cgi?id=425483
 */
public class Bug425483Test {

	private static ResourceSet resourceSet;
	private static Profile profile;

	@Test
	public void accessStereotypeType() {

		ProfileMetaModel profileMetaModel = createTypeSystem();

		// access stereotype
		String stereotypeName = "TestProfile425483::SomeStereotype";
		Type stereotype = profileMetaModel.getTypeForName(stereotypeName);

		// assertions
		Assert.assertNotNull(stereotype);
		Assert.assertTrue(stereotype instanceof StereotypeType);
		Assert.assertEquals(stereotypeName, stereotype.getName());
	}

	@Test
	public void accessEClassType() {

		ProfileMetaModel profileMetaModel = createTypeSystem();

		// access EClassType
		String stereotypeName = "TestProfile425483::SomeMetaClass";
		Type eClassType = profileMetaModel.getTypeForName(stereotypeName);

		// assertions
		Assert.assertNotNull(eClassType);
		Assert.assertTrue(eClassType instanceof EClassType);
		Assert.assertEquals(stereotypeName, eClassType.getName());
	}

	private ProfileMetaModel createTypeSystem() {

		ProfileMetaModel profileMetaModel = new ProfileMetaModel(profile);
		TypeSystemImpl typeSystemImpl = new TypeSystemImpl();
		typeSystemImpl.registerMetaModel(profileMetaModel);
		return profileMetaModel;
	}

	@BeforeClass
	public static void initializeUmlTooling() throws Exception {

		// load and define the profile
		resourceSet = new ResourceSetImpl();

		URL url = Bug425483Test.class.getResource("Bug425483.profile.uml");
		URI externalURI = URI.createURI(url.toExternalForm());
		EClass literal = UMLPackage.Literals.PROFILE;
		profile = (Profile) UML2Util.load(resourceSet, externalURI, literal);

		profile.define();
	}

	@AfterClass
	public static void cleanUp() throws Exception {

		for (Resource next : resourceSet.getResources()) {
			next.unload();
		}

		resourceSet.getResources().clear();
		resourceSet.eAdapters().clear();
		resourceSet = null;
	}
}
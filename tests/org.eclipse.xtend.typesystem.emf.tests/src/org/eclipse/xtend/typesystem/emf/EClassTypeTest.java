/*******************************************************************************
 * Copyright (c) 2005, 2006 committers of openArchitectureWare and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     committers of openArchitectureWare - initial API and implementation
 *******************************************************************************/
package org.eclipse.xtend.typesystem.emf;

import junit.framework.TestCase;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.xtend.expression.ExecutionContextImpl;
import org.eclipse.xtend.expression.ExpressionFacade;
import org.eclipse.xtend.expression.Variable;
import org.eclipse.xtend.typesystem.Property;

public class EClassTypeTest extends TestCase {
	private EmfRegistryMetaModel mm;
	private ExecutionContextImpl ctx;
	private ExpressionFacade ec;
	private EClass clazz;

	@Override
    protected void setUp() throws Exception {
		super.setUp();
		EPackage pack = EcoreFactory.eINSTANCE.createEPackage();
		pack.setName("test1");
		pack.setNsURI("http://test/ecore");
		
		clazz = EcoreFactory.eINSTANCE.createEClass();
		clazz.setName("AClass");
		pack.getEClassifiers().add(clazz);
		
		EPackage.Registry.INSTANCE.put(pack.getNsURI(), pack);
		
		mm = new EmfRegistryMetaModel();
		ctx = new ExecutionContextImpl();
		ctx.registerMetaModel(mm);
		ec = new ExpressionFacade(ctx);
	}

	/**
	 * A structural feature that is 'unsettable' offers an additional operation 'isSet...', which
	 * evaluates {@link org.eclipse.emf.ecore.EObject#eIsSet(org.eclipse.emf.ecore.EStructuralFeature)}.
	 */
	public final void test_unsettableAttribute() {
		// CREATE METAMODEL
		EAttribute attr = EcoreFactory.eINSTANCE.createEAttribute();
		clazz.getEStructuralFeatures().add(attr);
		attr.setName("unsettableAttribute");
		attr.setEType(EcorePackage.Literals.ESTRING);
		attr.setUnsettable(true);

		EAttribute attr2 = EcoreFactory.eINSTANCE.createEAttribute();
		clazz.getEStructuralFeatures().add(attr2);
		attr2.setName("settableAttribute");
		attr2.setEType(EcorePackage.Literals.ESTRING);
		attr2.setUnsettable(false);

		// ANALYZE EClassType
		EClassType type = (EClassType) mm.getTypeForEClassifier(clazz);
		assertNotNull(type);
		Property property = type.getProperty("unsettableAttribute");
		assertNotNull(property);
		
		// Bug#421957: isSetXXX operation for unsettable features
		assertNotNull("Unsettable feature must have a isSetXXX operation", type.getOperation("isSetUnsettableAttribute", null));
		assertNotNull("Unsettable feature must have a unsetXXX operation", type.getOperation("unsetUnsettableAttribute", null));
		assertNull   ("Unsettable feature do not offer a setXXX operation", type.getOperation("setUnsettableAttribute", null));
		
		
		// EVALUTE OPERATIONS
		// Now create an instance
		EFactory factory = EPackage.Registry.INSTANCE.getEFactory("http://test/ecore");
		EObject obj = factory.create(clazz);
		// Add object to ExecutionContext
		ec = ec.cloneWithVariable(new Variable("obj", obj));
		
		assertEquals(Boolean.FALSE, ec.evaluate("obj.isSetUnsettableAttribute()"));
		attr.setUnsettable(false);
		obj.eSet(attr, "Foo");
		attr.setUnsettable(true);
		
		// now the feature is set
		assertEquals(Boolean.TRUE, ec.evaluate("obj.isSetUnsettableAttribute()"));

		// call unset method
		assertNull(ec.evaluate("obj.unsetUnsettableAttribute()"));
		// now the attribute is unset again
		assertEquals(Boolean.FALSE, ec.evaluate("obj.isSetUnsettableAttribute()"));
		
		// try to set the value
		Object result = ec.evaluate("obj.setUnsettableAttribute('Bar')");
		assertEquals("Bar", ec.evaluate("obj.unsettableAttribute"));
		
		
		// Counter test: Settable features do not have an 'isSet' operation

		property = type.getProperty("settableAttribute");
		assertNotNull(property);
		
	}
	
}

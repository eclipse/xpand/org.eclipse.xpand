/*******************************************************************************
 * Copyright (c) 2012 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package org.eclipse.internal.xtend.expression.ast;

import junit.framework.TestCase;

/**
 * Check behavior of {@link Identifier}
 * 
 * @author thoms - Initial contribution and API
 */
public class IdentifierTest extends TestCase {
	private Identifier[] arg;

	public void testMemoryConsumption() throws Exception {
		final Runtime runtime = Runtime.getRuntime();
		System.gc();
		long memoryBeg = runtime.totalMemory() - runtime.freeMemory();
		arg = new Identifier[100000];
		for (int i = 0; i < arg.length; i++) {
			Identifier id = new Identifier("xtend").append(new Identifier("::")).append(new Identifier("test" + i % 10));
			// call getValue to fill 'fullValue' variable
			id.toString();
		}
		// now call GC again to measure effective consumption
		System.gc();
		long memoryEnd = runtime.totalMemory() - runtime.freeMemory();
		long consumedMem = (memoryEnd - memoryBeg) / 1024;

		// originally this consumed ~14177kB, see bug

		System.out.println(arg.length + " Identifiers consume " + consumedMem + " kB");
	}

	public void testMemoryConsumption2() throws Exception {
		final Runtime runtime = Runtime.getRuntime();
		System.gc();
		long memoryBeg = runtime.totalMemory() - runtime.freeMemory();
		arg = new Identifier[100000];
		for (int i = 0; i < arg.length; i++) {
			Identifier id = new Identifier("xtend").append(new Identifier("::")).append(new Identifier("test"));
			// call getValue to fill 'fullValue' variable
			id.toString();
		}
		// now call GC again to measure effective consumption
		System.gc();
		long memoryEnd = runtime.totalMemory() - runtime.freeMemory();
		long consumedMem = (memoryEnd - memoryBeg) / 1024;

		// originally this consumed ~14177kB, see bug

		System.out.println(arg.length + " identical Identifiers consume " + consumedMem + " kB");
	}

	public void testGetValue() {
		Identifier id = new Identifier("xtend").append(new Identifier("::")).append(new Identifier("test"));
		assertEquals("xtend::test", id.toString());
	}

	/**
	 * end is influenced by call of getValue()
	 */
	public void testEnd() {
		Identifier id1 = new Identifier("xtend");
		id1.setStart(1);
		id1.setEnd(5);
		Identifier id2 = new Identifier("::");
		id2.setStart(6);
		id2.setEnd(7);
		Identifier id3 = new Identifier("test");
		id3.setStart(8);
		id3.setEnd(11);

		// build id
		id1.append(id2).append(id3);
		assertEquals(11, id1.getEnd());

		// now call getValue and compare again
		id1.toString();
		assertEquals(11, id1.getEnd());
	}

	public void testAppend() {
		Identifier id1 = new Identifier("xtend");
		id1.setStart(1);
		id1.setEnd(5);
		Identifier id2 = new Identifier("::");
		id2.setStart(6);
		id2.setEnd(7);

		assertTrue("append must preserve identity", id1 == id1.append(id2));
		// append modifies end
		assertEquals(7, id1.getEnd());
	}

	public void testEscapeSign() {
		Identifier id = new Identifier("^xtend").append(new Identifier("::")).append(new Identifier("^test"));
		assertEquals("xtend::test", id.toString());
	}

	public void testNamespaceDelimiter() {
		Identifier id = new Identifier("xtend").append(new Identifier(".")).append(new Identifier("test")).append(new Identifier("."))
				.append(new Identifier("test2"));
		assertEquals("xtend.test.test2", id.toString());
		id = new Identifier("xtend").append(new Identifier("::")).append(new Identifier("test")).append(new Identifier("."))
				.append(new Identifier("test2"));
		assertEquals("xtend::test.test2", id.toString());
	}

	public void testCaching() {
		Identifier id1 = new Identifier("xtend");
		Identifier id2 = new Identifier("xtend");

		assertTrue(id1.getFirstSegment() == id2.getFirstSegment());
	}

}

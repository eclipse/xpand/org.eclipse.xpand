/*******************************************************************************
 * Copyright (c) 2013 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package org.eclipse.internal.xtend.util;

import junit.framework.TestCase;

/**
 * @author Karsten Thoms - Initial contribution and API
 */
public class StringHelperTest extends TestCase {

	public void testSplit() {
		assertEquals("[]", StringHelper.split("", ",").toString());
		assertEquals("[foo]", StringHelper.split("foo", ",").toString());
		assertEquals("[foo, bar]", StringHelper.split("foo,bar", ",").toString());
		assertEquals("[foo, bar]", StringHelper.split("foo::bar", "::").toString());
		assertEquals("[foo, bar, baz]", StringHelper.split("foo::bar::baz", "::").toString());
		assertEquals("[foo, bar:baz]", StringHelper.split("foo::bar:baz", "::").toString());
	}
}

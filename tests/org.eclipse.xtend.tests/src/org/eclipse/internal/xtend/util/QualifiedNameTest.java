/*******************************************************************************
 * Copyright (c) 2012 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package org.eclipse.internal.xtend.util;

import junit.framework.TestCase;

/**
 * Check behavior of {@link QualifiedName}
 * 
 * @author thoms - Initial contribution and API
 */
public class QualifiedNameTest extends TestCase {
	private QualifiedName[] arg;

	public void testMemoryConsumption() throws Exception {
		final Runtime runtime = Runtime.getRuntime();
		System.gc();
		long memoryBeg = runtime.totalMemory() - runtime.freeMemory();
		arg = new QualifiedName[100000];
		for (int i = 0; i < arg.length; i++) {
			QualifiedName qn = QualifiedName.create(new String[] { "xtend", "::", "test" + i % 10 });
			arg[i] = qn;
			// call toString to fill 'fullValue' variable
			qn.toString();
		}
		// now call GC again to measure effective consumption
		System.gc();
		long memoryEnd = runtime.totalMemory() - runtime.freeMemory();
		long consumedMem = (memoryEnd - memoryBeg) / 1024;

		// originally this consumed ~14177kB, see bug

		System.out.println(arg.length + " QualifiedNames consume " + consumedMem + " kB");
	}

	public void testMemoryConsumption2() throws Exception {
		final Runtime runtime = Runtime.getRuntime();
		System.gc();
		long memoryBeg = runtime.totalMemory() - runtime.freeMemory();
		arg = new QualifiedName[100000];
		for (int i = 0; i < arg.length; i++) {
			QualifiedName qn = QualifiedName.create(new String[] { "xtend", "::", "test" });
			arg[i] = qn;
			// call getValue to fill 'fullValue' variable
			qn.toString();
		}
		// now call GC again to measure effective consumption
		System.gc();
		long memoryEnd = runtime.totalMemory() - runtime.freeMemory();
		long consumedMem = (memoryEnd - memoryBeg) / 1024;

		// originally this consumed ~14177kB, see bug

		System.out.println(arg.length + " identical QualifiedNames consume " + consumedMem + " kB");
	}

	public void testToString() {
		assertEquals("xtend::test", QualifiedName.create(new String[] { "xtend", "::", "test" }).toString());
	}

	public void testToStringWithDelimiter() {
		assertEquals("xtend::test", QualifiedName.create(new String[] { "xtend", "test" }).toString("::"));
	}

	public void testResourceWithColonWithMultiCharDelimiter() {
		final String expected = "C:\\Some\\Path\\on\\the\\filesystem\\xtend::test";
		final QualifiedName qualifiedName = QualifiedName.create(expected, "::");
		assertEquals(expected, qualifiedName.toString("::"));
	}

	public void testOneSegment() {
		QualifiedName a = QualifiedName.create("foo");
		QualifiedName b = QualifiedName.create("foo", "::");
		assertTrue(a == b);
	}

	public void testAppend() {
		QualifiedName qn1 = QualifiedName.create(new String[] { "xtend", "::" });
		QualifiedName qn2 = QualifiedName.create(new String[] { "test" });

		assertEquals("xtend::test", qn1.append(qn2).toString());
	}

	public void testNamespaceDelimiter() {
		QualifiedName id = QualifiedName.create(new String[] { "xtend", ".", "test", ".", "test2" });
		assertEquals("xtend.test.test2", id.toString());
		id = QualifiedName.create(new String[] { "xtend", "::", "test.test2" });
		assertEquals("xtend::test.test2", id.toString());
	}

	public void testCaching() {
		QualifiedName qn1 = QualifiedName.create(new String[] { "xtend", "::", "test1" });
		QualifiedName qn2 = QualifiedName.create(new String[] { "xtend", "::", "test2" });
		QualifiedName qn3 = QualifiedName.create(new String[] { "xtend", "::", "test1" });
		assertTrue(qn1.getFirstSegment() == qn2.getFirstSegment());
		assertTrue(qn1 == qn3);
	}

}

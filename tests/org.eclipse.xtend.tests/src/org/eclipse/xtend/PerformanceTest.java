package org.eclipse.xtend;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.eclipse.internal.xtend.xtend.ast.ExtensionFile;
import org.eclipse.internal.xtend.xtend.parser.ParseFacade;

public class PerformanceTest extends TestCase {
	@SuppressWarnings("unchecked")
	public void testPerformance() throws Exception {
		final XtendFacade f = XtendFacade.create("org::eclipse::xtend::Performance");

		List<Object> strings = new ArrayList<Object>();

		for (int i = 0; i < 200000; i++) {
			if (i % 2 == 0) {
				strings.add(i);
			} else {
				strings.add("string" + i);
			}
		}
		// long before = System.currentTimeMillis();
		List<Object> result = (List<Object>) f.call("doStuff", new Object[] { strings });
		// long after = System.currentTimeMillis();
		assertEquals(strings, result);
		// long time = after-before;
		// assertTrue("expected 9000 but was "+time+" milliseconds", time<9000);
	}

	public void testXtendFacade() throws Exception {
		System.gc();
		long memBegin = Runtime.getRuntime().freeMemory();
		long timeBegin = System.currentTimeMillis();
		final int SIZE = 1000;
		List<ExtensionFile> extFiles = new ArrayList<ExtensionFile>(SIZE);
		for (int i = 0; i < SIZE; i++) {
			ExtensionFile file = createXtendFile(i, 10);
			extFiles.add(file);
		}
		System.gc();
		Thread.sleep(10); // wait for GC
		long memEnd = Runtime.getRuntime().freeMemory();

		System.out.println("Mem Usage: " + (memBegin - memEnd) / 1024 + " kB");
		System.out.println("Time Usage: " + (System.currentTimeMillis() - timeBegin));

	}

	private ExtensionFile createXtendFile(int index, int numExt) {
		StringBuilder b = new StringBuilder();
		final String prefix = "my::template::prefix::identifier::";
		b.append("import java::math;\n");
		if (index > 1) {
			b.append("import " + prefix + "Extension" + (index - 1) + ";\n");
		}
		for (int i = 0; i < numExt; i++) {

			b.append("String someExtension_" + index + "_" + i + " (String param1, Integer param2, Integer param3) : param1 + param2 + param3;\n\n");

		}
		return ParseFacade.file(new StringReader(b.toString()), "Extension" + index + ".ext");
	}
}

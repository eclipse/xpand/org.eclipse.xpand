/*******************************************************************************
 * Copyright (c) 2015 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package org.eclipse.xtend.shared.ui.test.core.preferences.internal;

import org.eclipse.xtend.shared.ui.core.internal.preferences.PatternDialog;

/**
 * @author thoms - Initial contribution and API
 */
public class PatternDialogTest  {
	public static void main(String[] args) {
		PatternDialog dlg = new PatternDialog(null);
		dlg.open();
	}
}

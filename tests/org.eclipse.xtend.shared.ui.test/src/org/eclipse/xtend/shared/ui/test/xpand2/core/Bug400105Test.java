/*******************************************************************************
 * Copyright (c) 2013 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package org.eclipse.xtend.shared.ui.test.xpand2.core;

import java.io.InputStream;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.Path;

/**
 * @author thoms - Initial contribution and API
 */
public class Bug400105Test extends XpandCoreTestBase {
	
	public void testBug400105 () throws Exception {
		env.openEmptyWorkspace();
		InputStream stream = getClass().getClassLoader().getResourceAsStream("/resources/bug400105.zip");
		env.importZippedProject(stream);
		env.fullBuild();
		
		IFile tplFile = ResourcesPlugin.getWorkspace().getRoot().getFile(new Path("/my.generator.project/src/OtherTemplate.xpt"));
		assertTrue(tplFile.exists());
		assertEquals("There must be no error", 0, env.getMarkersFor(tplFile.getFullPath()).length);
	}

}

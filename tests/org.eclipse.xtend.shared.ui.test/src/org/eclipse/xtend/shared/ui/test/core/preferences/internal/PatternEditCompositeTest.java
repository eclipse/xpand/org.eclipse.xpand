/*******************************************************************************
 * Copyright (c) 2015 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package org.eclipse.xtend.shared.ui.test.core.preferences.internal;

import org.eclipse.jface.window.ApplicationWindow;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.xtend.shared.ui.core.internal.preferences.PatternDialog;
import org.eclipse.xtend.shared.ui.core.internal.preferences.PatternEditComposite;

/**
 * @author thoms - Initial contribution and API
 */
public class PatternEditCompositeTest extends ApplicationWindow {
	public PatternEditCompositeTest(Shell parentShell) {
		super(parentShell);
	}
	
	@Override
	protected Control createContents(Composite parent) {
		Composite composite = new PatternEditComposite(parent, SWT.NULL);
		return super.createContents(parent);
	}

	public static void main(String[] args) {
		ApplicationWindow window = new PatternEditCompositeTest(null);
		window.setBlockOnOpen(true);
		window.open();
	}
}

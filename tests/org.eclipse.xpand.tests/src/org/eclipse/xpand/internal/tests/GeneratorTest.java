/*******************************************************************************
 * Copyright (c) 2005, 2009 committers of openArchitectureWare and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     committers of openArchitectureWare - initial API and implementation
 *******************************************************************************/
package org.eclipse.xpand.internal.tests;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import org.eclipse.xpand2.Generator;
import org.eclipse.xpand2.output.JavaBeautifier;
import org.eclipse.xpand2.output.Outlet;
import org.eclipse.xpand2.output.Output;
import org.eclipse.xpand2.output.PostProcessor;

public class GeneratorTest extends TestCase {
	class Generator2 extends Generator {
		public Output _getOutput() {
			return getOutput();
		}
	}
	private Generator2 generator;
	
	@Override
	public void setUp () throws Exception {
		generator = new Generator2();
	}
	
	public void testOutlets_DefaultEncoding () {
		configureDefaultOutlet();
		List<Outlet> outlets = generator.getOutlets();
		assertEquals(1, outlets.size());
		Outlet o = outlets.get(0);
		assertEquals(System.getProperty("file.encoding"), o.getFileEncoding());
	}
	
	/**
	 * The properties 'fileEncoding' and 'beautifier' must be configured before Outlets are configured, since
	 * they will be set when adding Outlets.
	 * 
	 */
	public void testOutlets_InvalidConfigOrder () {
		// choose encoding different from system default
		String encoding = System.getProperty("file.encoding").equalsIgnoreCase("utf8") ? "utf8" : "ISO-8859-1";
		configureDefaultOutlet();
		
		try {
			generator.setFileEncoding(encoding);
		} catch (IllegalStateException e) { 
			assertEquals("'fileEncoding' must be configured before any Outlet.", e.getMessage());
		}
		try {
			List<PostProcessor> p = new ArrayList<PostProcessor>();
			p.add(new JavaBeautifier());
			generator.setBeautifier(p);
			fail ("IllegalStateException expected");
		} catch (IllegalStateException e) { 
			assertEquals("'beautifier' must be configured before any Outlet.", e.getMessage());
		}
		
	}
	
	public void testOutlets_CustomEncoding () {
		// choose encoding different from system default
		String encoding = System.getProperty("file.encoding").equalsIgnoreCase("utf8") ? "utf8" : "ISO-8859-1";
		generator.setFileEncoding(encoding);
		
	}
	
	
	private void configureDefaultOutlet () {
		Outlet o = new Outlet();
		o.setPath(System.getProperty("java.io.tmpdir"));
		generator.addOutlet(o);
	}
	
	@SuppressWarnings("deprecation")
	public void testGetOutlets () {
		assertEquals(0, generator.getOutlets().size());
		generator.addOutlet(new Outlet("foo"));
		assertEquals(1, generator.getOutlets().size());
		generator.setGenPath("src-gen"); // adds two outlets
		assertEquals(3, generator.getOutlets().size());
		generator.setSrcPath("src"); // adds outlet "ONCE"
		assertEquals(4, generator.getOutlets().size());
	}
	
	public void testDefaultOutlet () {
		generator.addOutlet(new Outlet(null));
		Outlet defaultOutlet = generator._getOutput().getOutlet(null);
		assertNotNull("Default Outlet must be configured.", defaultOutlet);
	}
	
}

/*******************************************************************************
 * Copyright (c) 2012 itemis AG (http://www.itemis.eu) and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *******************************************************************************/
package org.eclipse.xtend.typesystem.xsd.tests.xmlbeautifier;

import java.io.File;

import org.eclipse.xpand2.output.FileHandle;
import org.eclipse.xpand2.output.Outlet;
import org.eclipse.xtend.typesystem.xsd.XMLBeautifier;
import org.eclipse.xtend.typesystem.xsd.tests.AbstractTestCase;

/**
 * @author Karsten Thoms - Initial contribution and API
 */
public class XMLBeautifierTest extends AbstractTestCase {

	private XMLBeautifier formatter = new XMLBeautifier();
	private static final String LINESEP_SYSTEM = System.getProperty("line.separator");
	
	@Override
	protected void tearDown() throws Exception {
		System.setProperty("line.separator", LINESEP_SYSTEM);
		super.tearDown();
	}
	
	class DummyFileHandle implements FileHandle {
		private CharSequence buffer;
		private File targetFile;
		
		public Outlet getOutlet() {
			return null;
		}

		public CharSequence getBuffer() {
			return buffer;
		}

		public void setBuffer(CharSequence buffer) {
			this.buffer = buffer;;
		}

		@Deprecated
		public File getTargetFile() {
			return targetFile;
		}

		public String getAbsolutePath() {
			return targetFile.getAbsolutePath();
		}

		public boolean isAppend() {
			return false;
		}

		public boolean isOverwrite() {
			return false;
		}

		public String getFileEncoding() {
			return "UTF-8";
		}

		public void writeAndClose() {
		}
		
	}
	
	public void testMixedContent () {
		System.setProperty("line.separator", "\n"); // set UNIX style default

		DummyFileHandle fh = new DummyFileHandle();
		fh.targetFile = new File(getSrcGenDir()+"/XMLBeautifier-Out-mixedContent.xml");
		StringBuilder in = new StringBuilder();
		in.append("<letter>Dear Mr.<name>John Smith</name>.");
		in.append("Your order <orderid>1032</orderid>will be shipped on <shipdate>2001-07-13</shipdate>.");
		in.append("</letter>");
		fh.setBuffer(in);

		StringBuilder expected = new StringBuilder();
		
		expected.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n");
		expected.append("<letter> Dear Mr.\n");
		expected.append("  <name>John Smith</name>. Your order\n");
		expected.append("  <orderid>1032</orderid> will be shipped on\n");
		expected.append("  <shipdate>2001-07-13</shipdate>.\n");
		expected.append("</letter>\n");
		fh.setBuffer(expected);

		formatter.beforeWriteAndClose(fh);
		assertFalse("See bug#356576", fh.getBuffer().toString().contains("&#xD;"));
		assertEquals(expected.toString(), fh.getBuffer().toString());
		
		// now test Windows style
		in = new StringBuilder(in.toString().replace("\n", "\r\n"));
		expected = new StringBuilder(expected.toString().replace("\n", "\r\n"));
		fh.setBuffer(expected);
		formatter.setLineSeparator("WINDOWS");
		System.setProperty("line.separator", "\r\n"); // set WINDOWS style
		// we have to set the system property also because the line break after the first
		// line will be with system default
		formatter.beforeWriteAndClose(fh);
		assertFalse("See bug#356576", fh.getBuffer().toString().contains("&#xD;"));
		assertEquals(expected.toString(), fh.getBuffer().toString());
		
	}
}
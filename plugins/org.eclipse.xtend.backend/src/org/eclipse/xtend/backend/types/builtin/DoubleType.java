/*
Copyright (c) 2008 Arno Haase.
All rights reserved. This program and the accompanying materials
are made available under the terms of the Eclipse Public License v1.0
which accompanies this distribution, and is available at
http://www.eclipse.org/legal/epl-v10.html

Contributors:
    Arno Haase - initial API and implementation
 */
package org.eclipse.xtend.backend.types.builtin;

import org.eclipse.xtend.backend.common.BackendType;
import org.eclipse.xtend.backend.common.ExecutionContext;
import org.eclipse.xtend.backend.common.FunctionDefContext;
import org.eclipse.xtend.backend.common.QualifiedName;
import org.eclipse.xtend.backend.types.AbstractType;


/**
 *
 * @author Arno Haase (http://www.haase-consulting.com)
 * @author Karsten Thoms - Bug#297613
 */
public final class DoubleType extends AbstractType {
    public static final DoubleType INSTANCE = new DoubleType();

    private DoubleType () {
    	super ("Double", "{builtin}Double");
    	register(new QualifiedName("toInteger"), new BuiltInOperation("toInteger",this) {
			private FunctionDefContext fdc;
			public void setFunctionDefContext(FunctionDefContext fdc) {
				this.fdc = fdc;
			}

			public Object invoke(ExecutionContext ctx, Object[] params) {
				Double d = (Double) params[0];
				return d.longValue();
			}

			public BackendType getReturnType() {
				return LongType.INSTANCE;
			}

			public FunctionDefContext getFunctionDefContext() {
				return fdc;
			}
		});
    }

    @Override
    public boolean isAssignableFrom (BackendType other) {
        return other == this || other == VoidType.INSTANCE;
    }

    @Override
    public boolean equals (Object other) {
        return other == this;
    }


}

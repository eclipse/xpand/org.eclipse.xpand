=============================
= XPAND RELEASE ENGINEERING =
=============================

= Set Release Version =

The Ant script versions.xml contains the task "set-version". The parameter "newVersion" has to be passed to
the target. Run launch config 
  org.eclipse.xpand.releng versions.xml.launch

